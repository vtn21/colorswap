#!/usr/bin/env python

import rospy
import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import PointCloud2, PointField

def callback(pc):
    data_out = pc2.read_points(pc, field_names=None, skip_nans=False)
    points = []
    for i in range(pc.width):
        point = next(data_out)
        temp = (point[0], point[1], point[2], 255.0 - point[3], point[4])
        points.append(temp)
    new_pc = pc2.create_cloud(pc.header, pc.fields, points)
    rate = rospy.Rate(100)
    pub = rospy.Publisher('velodyne_points2', PointCloud2, queue_size = 100)
    pub.publish(new_pc)
    rate.sleep()

def swap():
    rospy.init_node('swapper', anonymous = True)
    rospy.Subscriber('velodyne_points', PointCloud2, callback)
    rospy.spin()

if __name__ == '__main__':
    try:
        swap()
    except rospy.ROSInterruptException:
        pass
